﻿#include <iostream>
#include <string>


class Player
{
public:
    std::string name;
    int points;
};


int main()
{

    int PlayerNumber;//кол-во игроков, размер массива с игроками
    std::cout << "How many player do you want?:\n";//вывод сообщения в консоль
    std::cin >> PlayerNumber;//ввод размера массива
    //Выделение памяти для объектов класса игрок
    //Создаем массив указанного размера. Тип массива - объекты класса игрок
    Player* PlayerArray = new Player[PlayerNumber];


    //Цикл. В нем проходимся по всем объектам в массиве, задаём им имя и количество очков
    for (int i = 0; i < PlayerNumber; i++)
    {
        std::cout << "Enter Player Name:" << i + 1 << "\n";
        std::cin >> PlayerArray[i].name;
        std::cout << "Enter Player Score:" << i + 1 << "\n";
        std::cin >> PlayerArray[i].points;
    }

    //Итоговый вывод значений, которые ввели
    for (int i = 0; i < PlayerNumber; i++)
    {
        std::cout << PlayerArray[i].name << " " << PlayerArray[i].points << "\n";
    }

    //Код для сортировки
    for (int i = 0; i < PlayerNumber; i++) //этот цикл отработает столько раз, сколько элементов в массиве
    {
        //этот цикл будет перебирать элементы от i до начала цикла и сравнивать кол-во очков у игроков
        for (int j = i; j > 0 && PlayerArray[j].points > PlayerArray[j - 1].points; j--)
        {
            //здесь условие: если у предыдущего игрока очков БОЛЬШЕ, то меняем их местами
            std::swap(PlayerArray[j - 1], PlayerArray[j]);
        }
    }

    //Код для вывода результата сортировки
    std::cout << std::endl;

    for (int i = 0; i < PlayerNumber; i++)
    {
        std::cout << PlayerArray[i].name << " " << PlayerArray[i].points << "\n";
    }

    //работа с памятью
    delete[] PlayerArray;
    PlayerArray = nullptr;
    std::cout << std::endl;

}

